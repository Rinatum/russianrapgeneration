# RussianRapGeneration

In this repo I tried to solve such interesting problem as russian rap generation.
I tried to parse website with data and using it to fit most modern Language Models

## Installation

`pip install -r requirements.txt`

## Check the results

* Parsing process is in the *notebooks/parse.ipynb*
* Training pipeline is in the *notebooks/train.ipynb*
* All weights are in repo so you can just play with models in *notebooks/inference.ipynb*