import re

from nltk.tokenize import WordPunctTokenizer, WhitespaceTokenizer
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords
import nltk

nltk.download('wordnet')
nltk.download('stopwords')


class Preprocessor:

    def __init__(self):
        self.tokenizer = WordPunctTokenizer()
        self.lemmatizer = WordNetLemmatizer()

    def __call__(self, text):
        result = text
        for preprocessing_func in [self.normalize, self.tokenize, self.lemmatize, self.remove_stop_word]:
            result = preprocessing_func(result)
        return result

    def normalize(self, text):
        ret = [i for i in re.split('\W', text.lower()) if i.isalpha()]
        ret = ' '.join(ret)
        return ret

    def tokenize(self, text):
        return self.tokenizer.tokenize(text)

    def lemmatize(self, tokens):
        return [self.lemmatizer.lemmatize(token) for token in tokens]

    def remove_stop_word(self, tokens):
        stopWords = set(stopwords.words('english'))
        return [i for i in tokens if i not in stopWords]
