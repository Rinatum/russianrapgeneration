
Че, оставляем так или перечитать?
2012
Так срослось, добро это или зло,
Но мне серьезно повезло, что есть такое музло.
Час за часом, день за днем, неделя за неделей
Я становлюсь значительно сильнее.
Сугробы постепенно размывает проливным дождем,
И стал виден еле-еле свет в конце тоннеля.
Я снова в деле, руки, ноги вроде целы.
Со временем как на собаке заживают раны.
Некоторые конкуренты осели в недоумении.
В голове сами собой рождаются коварные планы.
Не на того напали, парвни, ошибочка вышла.
С чего вы взяли, что можно называть меня братишкой,
И почему решили, что имеете право решать за всех.
Я медленно прямо, а кто-то быстро наверх.
С этим смириться трудно. Все вроде с удовольствием
Пляшут под одну дудку, но не он почему-то.
Этот друг оказался продуманным фруктом,
Его любят, он отсюда и в курсе, че к чему тут.
Он может все испортить кому-то в одну секунду,
Может как-то насолить этому ублюдку.
Ну подкинуть ему в Хонду сверток с белой пудрой
Или припугнуть по-лютому, ну как-то так, ну да.
Если надо, я постоянно рядом.
Обладаю битами и правдой, помогает борода.
Болванка круглая, коробка квадратная.
Леша Долматов, два ноль один два.
— Че, оставляем так или перечитать?
— Пох, оставили. Поехали.
А я уже забыл, насколько это здорово:
Осознавать, что записанный трек оказался взорван.
Входя из микрофона. И хотеть е*ашить дальше.
Когда это по-настоящему, когда от рэпа таращит.
Для меня важно, чтобы все было именно так.
Я, скажем, не могу себя заставить написать,
А иногда вообще могу молчать по году,
Просто что-то не то, это может быть что угодно.
Пока я не почувствую ветерок куража,
Я не делаю лажу искусственную, продолжаю ждать.
Не напишу и пару строк за огромный срок.
Езжу и радуюсь качеству новых дорог,
Я про асфальт, который опять поменяли,
Спасибо, собяне. Но тут приходит напоминание.
Я не знаю, правильно ли я объясняю.
Как будто тебя позвали на третье крутое свидание.
Когда каким-то образом заранее знаешь,
Что сделаешь что-то реальное, это непередаваемо.
А на обратном пути со студии послушать демку с друзьями.
И если всем понравилось, вот это кайф.
Я люблю делать это с азартом,
И все время действую по традиции наугад.
Болванка круглая, коробка квадратная.
Леша Долматов, два ноль один два.
