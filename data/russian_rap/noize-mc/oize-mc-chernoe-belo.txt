
Давай прикинем, давай представим
Что, если белый и черный поменять местами.
Давай представим, давай прикинем
Посмотрим на мировую историю в негативе.
Давай прикинем, давай представим
Что, если белый и черный поменять местами.
Давай представим, давай прикинем
Посмотрим на мировую историю в негативе.
Давай прикинем, вот это был бы удар
Африка вместо Европы, Бутания — Мадагаскар.
Вместо Германии, например, пусть будет Эфиопия
Давай заценим такую антиутопию.
Черные колонизаторы на берегах Европы,
Эй, беложопые — айда собирать хлопок.
Полные трюмы невольников, торговля рабами
Хозяева ржут над узкими носами и тонкими губами.
Ха-ха-ха, вот умора
В рабство угоняют всех подряд без разбора.
Француз и немец или какой другой абориген,
Все белые на одно лицо один хрен.
Давай представим чисто в формате глума,
Черного Америго Веспуччи, Христофора Колумба.
Тут наш негативный снимок дает сбой, мой друг
Как ни крути, индейцам так и так каюк.
Давай прикинем, давай представим
Что, если белый и черный поменять местами.
Давай представим, давай прикинем
Посмотрим на мировую историю в негативе.
Давай прикинем, давай представим
Что, если белый и черный поменять местами.
Давай представим, давай прикинем
Ну ладно, что там было дальше. Если вкратце,
1860-ые — отмена рабства.
Момент конечно поворотный, это бесспорно
Белые теперь не звери, а люди третьего сорта.
1930-ые. Ни фига себе,
Черный Гитлер у власти вместо Хале Селайсе.
Воедино схоже сливается СС-совская униформа,
Че? Проверим, кто тут самый черный?
Трепещите кенийцы, больше нигериец
Мы вас сейчас покажем, кто тут истинный ариец.
... на мусор вырежем, как больные гланды
Трубы концлагерей коптят небо над Угандой.
Тут негатив тоже невозможен прямо скажем,
Стены печей перепачканы разноцветной сажей.
Что же напасть такая? В чем же дело?
Наверно просто черный это не антибелый.
Давай прикинем, давай представим
Что, если белый и черный поменять местами.
Давай представим, давай прикинем
Посмотрим на мировую историю в негативе.
Давай прикинем, давай представим
Что, если белый и черный поменять местами.
