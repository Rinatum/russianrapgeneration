
Нервно косы в волосы заплетала,
Верила в вещие сны
Часто вдохновения было мало,
Поэтому ждала запах весны 
Утро из пакета вымятой одежды
Свежий ветер проскользнет через балкон
Растеряла все и только в паузах где-то между
В тихой комнате закричит телефон 
По тротуарам город пыль пинает,
Решетки на окнах первых этажей
Она открыта, но ее никто уже не читает,
А кто прочел уже давно забыл о ней 
Она, она, уставшая, забытая
Она, она, кому никто, кому-то бывшая
Она, она, уставшая, забытая
Она, она, кому никто, кому-то бывшая 
Годы-то идут, годы-то не ждут,
Превращает время во второсортный продукт
Да тут и мама ежедневно капает,
Внуков требует, пока держит ее табурет 
Красными нитями сквозь жизнь мало
Что прошло было и хорошее, но его мала
Знала бы тогда, что упорхнет в окно,
Счастье то, которое не замечала 
Был мужик один, все цветы дарил,
Все ходил вокруг, в любви клялся
Оказалось что всех ее подруг он перелюбил,
Денег одолжил и съебался 
Она, она, уставшая, забытая
Она, она, кому никто, кому-то бывшая
Она, она, уставшая, забытая
Она, она, кому никто...
