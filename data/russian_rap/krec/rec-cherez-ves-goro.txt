
Я лечу к тебе через весь город,
У нас есть повод с тобой,
Ровно год прошел с того момента,
Когда я повстречал тебя, детка.
В пакете вино и твой любимый торт,
В другой руке букет шикарных роз,
До той премьеры еще далеко,
Но я уже пробил нам два билета в кино.
Не бегу, а лечу, а может все это сон,
Ущипните меня, ах, спасибо.
Тупая улыбка не покидает лицо,
Ведь в кармане футляр, а в футляре кольцо.
Как солнце, моя мечта дарит мне свет свой,
Даже ночью сердце шепчет, что я буду с тобой.
Как солнце, моя мечта дарит мне свет свой,
Даже ночью сердце шепчет, что это любовь.
Я лечу к тебе через весь город,
У нас есть повод с тобой,
Ровно год прошел с того момента,
Когда я повстречал тебя, детка.
Несусь к тебе, обгоняя трамваи,
Только дождись, прошу, родная.
Звоню на домашний, но он занят,
Возьми трубку, кричу, Зая. 
Несусь, острые углы, срезая,
Я знаю точно, не опоздаю.
Весь лоб мокрый, как будто в спортзале.
Беги! Иначе совесть тебя растерзает
Ты не злая. Нет! Конечно, нет.
Ты святая и все дело во мне,
Опять, я каюсь во всех грехах
На свете, детка, я виноват.
Я лечу к тебе через весь город,
У нас есть повод с тобой,
Ровно год прошел с того момента,
Когда я повстречал тебя, детка.
