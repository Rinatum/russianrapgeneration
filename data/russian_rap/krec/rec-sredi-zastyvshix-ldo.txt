
Припев:
Пока я жив я буду помнить,
Не забывай и ты меня,
Пока я жив я буду помнить,
Я поднимаю за тебя этот бокал вина, но без тебя...
Check
Далёкий путь, друзья уходят в поднебесье,
А здесь люди прячутся под маскою агрессии,
Согреться бы об твою руку в этой круговой поруке,
Но ты молчишь не отвечаю другу...
Глаза как лёд застыли, но душа летит,
От жизни мы отпили свой глоток, теперь бокал разбит,
И я разбит—как тот бокал разбит на части,
Эпизод настал, потерянный в последней части,
Затерянных мыслей бермудский треугольник,
Куда уходят корабли? Туда где их никто не вспомнит...
Только любовь растопит застывший лёд воспоминаний,
Мой друг ты здесь я чувствую твоё дыханье...
Припев:
Пока я жив я буду помнить,
Не забывай и ты меня,
Пока я жив я буду помнить,
Я поднимаю за тебя этот бокал вина, но без тебя...
Check
Хочется вспомнить всех, но много имён стёрто,
Остались лишь обрывки памяти и пару старых фото,
Хорошие люди уходят раньше чем плохие,
Я верю к лучшему всё—их от оков освободили,
Летите ангелы, летите прямо к небесам,
Быстрее! Выше! Летите к нашим праотцам,
Я верю вас там ждут и видят свет ваших очей,
Сад Эдэма откроет тайну для своих детей,
Но на земле...здесь нету никого родней,
Пусть пламя свечи согреет слёзы ваших матерей...
И где-то там—где нет формы и чисел,
Я на земле молюсь чтобы открылся вам священный смысл...
Раз оттуда никто не вернулся, значит там хорошо,
Светит солнце и дети смеются, там светло и тепло,
Раз оттуда никто не вернулся, значит там хорошо,
Светит солнце и дети смеются, там светло и тепло,
Припев:
Пока я жив я буду помнить,
Не забывай и ты меня,
Пока я жив я буду помнить,
Я поднимаю за тебя этот бакал вина, но без тебя...
Фьюз
Я буду помнить лучшие дни и твой старый номер
Буду Хранить в своей груди и своем телефоне
Мне тяжело и больно, глупо искать виновных
Кто я такой что бы судить, на все есть Божья воля
К близким с любовью, врагам оставь презрение
Все будет ровно, ты знаешь нужно время
Поднимем вверх бокалы, помянем добрым словом
Я позабочусь обо всем мой друг, спи спокойно
Больно, прими эти черные цветы
Наших жизней хрупкие мечты
Я сохраню навсегда, пока я жив я буду помнить
Припев:
Пока я жив я буду помнить,
Не забывай и ты меня,
Пока я жив я буду помнить,
Я поднимаю за тебя этот бокал вина, но без тебя.
