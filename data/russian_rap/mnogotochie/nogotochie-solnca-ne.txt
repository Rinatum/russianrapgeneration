
Для тех, кто далеко...Для тех, кто там...
Солнца нет...Под навесом судьбы...Ища ответ...Мы сжигаем мосты...Лишь на дне...Этой мутной воды...Видим суть...Не оставив следы...Солнца нет...Солнца нет...Вот ответ...Солнца нет...
Где же ответ на те вопросы,Что мешают по ночам спать?Слезы прятать в черствую ладонь,Разве это он?Разве эта белая дорога ведет к небу?Разве этот белый дым укажет мне туда дорогу?Нет больше солнца в моих тусклых глазах.Будучи ребенком, я прятал свои слезы в рукахИ мой страх для чужих голов теперь — скрежет плах!Я несу его тем, с кем мы на разных берегах...
Солнца нет...Солнца нет...Вот ответ...Солнца нет...Солнца нет...Солнца нет...Вот ответ...Солнца нет...
Для тех, кто далеко...Для тех, кто там...
