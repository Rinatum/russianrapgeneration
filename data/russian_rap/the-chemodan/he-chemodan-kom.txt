
Кома...
Скажи мне, сколько платят за падение с обрыва?
Жизнь лишь автопати после большого взрыва.
И с чего у Альпачино вечно закатаны веки?
И чем питался Ной на Ноевом ковчеге?
На дне детство топим в водке и вине
Тут кто-то на мели, а кто-то на глубине.
Всюду и ни в каком живем режиме
Торчок или на жиме. Если умный, то скажи мне.
Дождемся ли ответа, будет ли он услышан
Смысла тут может и нету, но я его вижу.
Иди же, а я останусь таким же
Утопая в той жиже все глубже, все ниже.
Есть зачем жить и есть за что умирать,
Все это странно и мне непонятно вдвойне.
Зачем и почему, хоть одну причину, знай
Захлебнулся в ванной тот, кто выжил в войне.
Вдоль дороги по всей ширине
Снова иду к тем, кто был на той стороне.
Хочу прийти и устав прочитать в их устах
Что нам приготовил тот мужик на небесах.
Бывает и маньяк бесполый.
Поможет ли маяк слепому?
Так кто же подскажет название дома,
Тех, кому знакомо состояние комы?
Очередная аксиома.
Поможет ли маяк слепому?
Но никто не подскажет название дома,
Где те, кому знакомо состояние комы.
Под потолком одним дымом дышим,
Заполняем собой ниши от подвала и до крыши.
И это утро казалось слишком муторным
Руку протяни и нам, по крови плывут они.
Грызешь локти. Толпа потоком из-под ногтя
Мы в меде бочка дегтя, краски стены не портят.
Чего хотят эти люди от этих ребят?
Слова взрывом петард. Смазливым для духа в пятак.
Видно сразу кто за кого тут тянет мазу
Погнал на базу и здесь даже не пахнет газом.
Раз за разом видим, браза, тут одна трасса
Кто-то по матрасам, калом массы в мясе.
Вскроет мечту консервным ножом,
Кто-то ниже этажом и никто не поражен.
Кома. Теперь ты знаешь мой номер дома.
Заходи, будем знакомы. Только веди себя скромно.
Бывает и маньяк бесполый.
Поможет ли маяк слепому?
Так кто же подскажет название дома,
Тех, кому знакомо состояние комы?
Очередная аксиома.
Поможет ли маяк слепому?
Но никто не подскажет название дома,
Где те, кому знакомо состояние комы.
