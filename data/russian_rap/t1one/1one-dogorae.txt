
Она сидит в кухне, на полу, одна
И в ветер веет сквозняком из приоткрытого окна
Сигареты стали частью её одиночества
Смотрит, как догорают, скоро они закончатся
Как и она, он словно выкурил её до пепла
Сделал последний вздох глубокий и швырнул гневно
Словно окурок на грязный асфальт и ушел
Не задумываясь, вынул новую из пачки он
Просто однажды она шла одна по улице
Проблемы у неё в душе холодным ветром кружатся
И вдруг взгляд, снова он и опять в глазах
Случайное знакомство или банальный азарт?
Но согласилась и дала свой номер телефона
Поняла — влюбилась, как рассвет сменяет ночь тёмную
Встречи, поцелуи и она уже мечтает
Как они поженятся и вместе детей воспитают
Он для неё – всё. Он для неё — свет
Она влюблена, но дым сигарет
Напоминает, всё прошло, и его нет
Пепел падает на пол, как холодный, белый снег
Она живет воспоминаниями каждый день
Уже отпустила и только серая тень
Осталась на стене, она её прогнать не может
Силуэт перед глазами и мороз по бледной коже
Выкурил до фильтра, не стряхивая пепел
Выпустил из себя, как дым, её унес ветер
Не может уснуть, даже на рассвете
Как наступает новый день, она не заметит
Как солнце ярко светит и прекрасен этот мир
Что же она упустила? Почему он остыл?
Казалось будто бы у них одно и тоже сердце
Одни и те же мысли и никуда не деться
От воспоминаний, как ходили в кино
Последний ряд, её обняв, он шепчет ей про любовь
И в лепестками усыпанной, комнате пили вино
Она прокручивает эти моменты, но
Всё решено и нет пути назад
Всё решено и потухает взгляд
Если она заплачет, это будет навсегда
Дым сигареты, ночь и одинокая луна
Она умна и красива, но не нужна никому
И роняет на футболку горькую, прозрачную слезу
Так хочется вернуть эту, неповторимую весну
И говорить ему, до потери пульса, слово «люблю»
Для него она была лишь, как очередная цель
Но в жизни будет еще настоящая любовь, поверь
Не закрывай в свою наивную душу стальную дверь
Хоть понимаю, что в твоей душе сейчас не оттепель
Она сидит в кухне, на полу, одна
И в ветер веет сквозняком из приоткрытого окна
Сигареты стали частью её одиночества
Смотрит, как догорают, скоро они закончатся
Как и она, он словно выкурил её до пепла
Сделал последний вздох глубокий и швырнул гневно
Словно окурок на грязный асфальт и ушел
Не задумываясь, вынул новую из пачки он
