
Эта жизнь движется только вперед, независимо от того,
Сначала мы хотим стать старше, затем быть вечно молодыми
Хотим любить или отталкиваем от себя любовь
Ненавидим, но прощаем в который раз
Мечтаем независимо от возраста и храним моменты счастья
И неисчерпаемую горечь по утраченным, до последнего дня
Городская тоска вновь придёт и слышен улиц стон
Снова пурга заметёт меня со всех сторон
Одиноко, но может суждено мне пройти через всё
Пусть освещает путь мне он
Я скитаюсь между слов, давно погасших во тьме
И каждый день похож на прежний в этом календаре
И я один над этим миром, ветер в птичьем крыле
Звёзды во льду на небе светом ярким блещут во тьме
Как дым не торопясь уходит на конце сигарет
Гоняет ветер, поплыли обрывки старых газет
Кто-то в твоей душе оставит незамеченный след
Город окутал тополиный пух как будто бы снег
Я соберусь на крышу дома, посмотрю свысока
На все проблемы, подомной дорога, будто река
Я напишу пару строк в блокнот, чем дышат города
Когда зажгутся фонари, как наша жизнь коротка
Хочу, как пёс дворовый сорваться с цепи, убежать
Но сердцу не прикажешь, продолжает крепко держать
Я затаю внутри себя от всех земную печаль
Закрою сердце и поставлю на затвору печать
Городская тоска вновь придёт и слышен улиц стон
Снова пурга заметёт меня со всех сторон
Одиноко, но может суждено мне пройти через всё
Пусть освещает путь мне он
Ночные фонари обыденных будней
Я с ними будто одинокий ветер мысли давай же остудим
Всё что пылает внутри, посмотри ты в мои глаза
Одиночество свобода, годы летят в небеса
Мне бы побыть там, где я буду один
Небо готово дарить дым парит с ним
Я буду читать до боли, что бы понять, что эта музыка чего-то стоит
Не надо лишних разговоров и моя дорога домой
Будто бы млечный путь, что по пути идёт годы со мной
Я попадаю в час пик, нас в миг порой это душит
Плевать на все эти висми, меня свобода кружит
Может, останусь один, либо ночной неон
Не покинет меня до рассвета, не погрузит меня в сон
И снова мысли кристально чисты, нервы в порядке
Ведь свобода так близка, я отдаюсь ей без остатка
Строки на лист под луной, днём звуковые дороги
Дороги мне, будто бы моего бытия истоки
Городская тоска вновь придёт и слышен улиц стон
Снова пурга заметёт меня со всех сторон
Одиноко, но может суждено мне пройти через всё
Пусть освещает путь мне он
