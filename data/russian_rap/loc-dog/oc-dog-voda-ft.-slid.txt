
Дай жизни берегам, влаги дай сухим губам и тишины,
Дай правды, я уже видел обман твой.
Милая волна, спой мысли и разбей о риф.
Я под тобой и мой тариф нулевые,
Под водой ни слова не слышно, кто что говорит.
И в одиночестве, дарованном Богом,
Я воздух города меняю на то, что не покорить.
Не горит мой мир, но того единый алгоритм снов,
Музыку слыша, двигайся в ритм с нами.
Мы тихо замерли в поиске вышины.
Где же мы, где же мир, не делай так, чтобы мир замер.
Так чтобы все напряжены, до света луны, сети сожжены,
Хотя и старая, но ожили с каплями.
Чудеса творить — твоя затея, суши дари для себя.
Напои, утопи, выливая, освежая, под солнцем не охлади.
Ты же больше чем суша, ты в курсе, зачем есть приливы,
Крик не глуши, дай дышать хоть чуть-чуть,
Ну же сделай меня исключением.
Напои, унеси своим бурным течением...
Внутри меня течет река, и не понять никак на берегах.
Нам некогда играть, и где та грань, не наступать на край.
Меня очистят дождевой водой и высушат ветра,
После падения на дно лишь снова научусь летать.
Она всё помнит тут за миллионы лет назад,
Помнит туман, помнит, как залечить собой десятки травм.
Запахом трав манят луга, но тянет глубина к себе,
Родной мой дом каждое утро утопает в темноте.
На теле только следы! Давайте перейдем на ты,
Давай пополним ряды тех, кто реально смогли
В душу пустую пустить только смех, радость от прошлых побед,
И есть или нет, не важно всё это бред.
И в мире только она на судне перепутье настоящим путь,
А рядом люди не судят, но не дают наполнить грудь.
И где мой брат подобрал ключи, что потушили запал.
Все это в голове творилось, пока я втыкал.
Напои, утопи, выливая, освежая под солнцем, не охлади.
Ты же больше чем суша, ты в курсе, зачем есть приливы,
Крик не глуши, дай дышать хоть чуть-чуть,
Ну же сделай меня исключением.
Напои, унеси своим бурным течением...
