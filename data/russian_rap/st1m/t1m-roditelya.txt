
Мне 25, но в душе я до сих пор пацан
И по-прежнему хочу во всём походить на отца
Он для меня тот, кем я восхищаюсь искренне
Рядом с ним в любом вопросе никого и близко нет
Не бизнесмен, не олигарх, простой водитель
Но чтобы кто-то так любил родных, я не видел
Он сделал всё, чтоб моё детство было самым-самым
Научив не бегать от трудностей, а смотреть в глаза им
Даже уставшим приходя домой после работы
Он со мной часами возился и окружал заботой
Я хочу быть для своих детей таким, как он
Благодаря ему я не боюсь летать так высоко
Уверен, мама ни разу не пожалела, что вышла за папу
Любовь — это когда плевать на статус и зарплату
И я горжусь, что вырос в самой счастливой семье
Дороже их у меня нет!
Пускай сменяются дни
Пусть пролетают года
И только этой любви
Не постареть никогда
Пускай сменяются дни
Пусть пролетают года
И только этой любви
Не постареть никогда
В душе я всё еще ребенок, хотя 25 уже
Но видя маму, как и в детстве, улыбаюсь до ушей
И все заботы уходят на третий план от ее объятий
И мне вдруг снова 7 и мы в той однушке в Тольятти
Она готовит ужин, а я жду папу с ВАЗа
И от этого на сердце так тепло сразу
В кухне ароматный запах домашней еды
А трубы фабрик за окнами выпускают дым
Она привила мне любовь к музыке и стихам
Но до сих пор переживает за меня по пустякам
Я пообедал, мам, и выспался, всё хорошо
Твой сын со всем справится, твой сын уже большой
И слыша в телефонной трубке их голоса
Я всегда спешу им что-нибудь хорошее сказать
И я горжусь, что вырос в самоей счастливой семье
Дороже их у меня нет!
Пускай сменяются дни
Пусть пролетают года
И только этой любви
Не постареть никогда
Пускай сменяются дни
Пусть пролетают года
И только этой любви
Не постареть никогда
Пускай сменяются дни
Пусть пролетают года
И только этой любви
Не постареть никогда
Пускай сменяются дни
Пусть пролетают года
И только этой любви
Не постареть никогда
