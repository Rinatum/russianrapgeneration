
Год назад я сидел на скамейке
В общественном парке на углу Beckton и Barking,
Думал, как из Англии выйти пешкою в дамки, к верху иерархии,
Я себе все это накаркал.
Теперь меня слушают хипстеры. Арт-богема?
«Ведь он из Лондона, где выставки, Портобелло.»
Не понимая, что внутри строки наболело,
Не понимая, что внутри смолит Карфагеном.
Жополизы, недруги, журналисты,
Первые компромиссы, лейблы-монополисты.
Я лишний, будто новички на вакханалии.
У всех, кто повыше, воротнички накрахмалены.
Был самодел, самопал, кто-то не захотел, сам отпал.
Мы — в отель, вам — подвал.
Но тут чертополох, тут если не успел, то подох.
Там, где ты видишь успех, я — подвох.
Ощущаю себя стариком, хоть просыпаюсь со стояком.
Не спасет благодать ста икон от сознания.
Комплексы съедают изнутри, я зову это «комплексное питание».
От бетона из Лондона на cover билбордов.
В клубе на Малой Бронной блюю под Ивана Дорна.
Зависаю с бомондом, засыпаю в уборных под хлорофилом,
Но чаще под хлороформом
На дессерт рвота с Heineken,
Боже, как же я себя люблю — Отто Вайнингер.
Моя жизнь — это приключения Незнайки,
Наизнанку, будто шлюха в «American psycho».
Эстеты напридумывали терминов, не зная, что выгуливали цербера.
«Наконец-то русский рэп признали» — мне не похуй ли?
Субкультура разрастается — опухоль.
Подземка не принимает — и бог с ней.
Эстрада напоминает клубок змей.
Я, как итог, — злей.
Все играют роли косплей, моя трасса уже не слалом, а бобслей.
Перебираю тщетно аллегории,
Меж тем моя любовь ебется с кем-то в Черногории.
Мне нужен врач, лучше на дом, и немедленно.
Хаус. Дре. Живаго. Менгеле.
Я подарю свою шкуру, кому по нраву.
Цели, до которых дошел, не дают отраду.
С Е16, где на углу продают отраву.
Говорун на левом плече, Гамаюн — на правом.
Уже зовут большие дяди на пикник афиши
После меня там читает Влади, мы фит запишем.
И все вокруг замечательно,
Но убей меня, пока я не скурвился окончательно.
Я всегда начинаю самоуверенно,
Но забиваю хуй и тону у самого берега.
Они жаждут подгон, а я все решаю,
Кто я — андеграунд или продажный гондон.
Дева не давала в жопу, говорила — принцип.
Но это во мне убило принца, как Гаврило Принцип.
Мироздание дремлет, но кажется, дождь собирается.
Очень приятно, Гремлин.
