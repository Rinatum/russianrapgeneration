
Шелест старых телеграмм,
Сердце с треском пополам,
Это Коля Сканк a.k.a Большая Тяга,
На фоне птенчиков застенчивых, я как бродяга,
Как вы кровь решили пить, если клыки из мармелада,
Я плачу лишь, когда на тот свет провожаю брата.
Дьявол предлагает мне контракт и предлагает бабки,
Хочет испытать меня с изнанки,
Люди примерные, снаружи грязные, внутри как панки,
Раздают себе разряды, титулы и ранги.
Я после себя оставлю ваших кумиров останки,
Трахну баб от Гидропонки до Бьянки, Фальшивым МС
Средние фаланги, вы за копейки ебётесь с продюсерами,
Как нимфоманки, любишь бодаться, люби получать в ебло,
Не раз собьёшь общественных стереотипов ледяные замки,
Беги под юбку мамки, поджавши хвост,
Я произношу за упокой вашего рэпа тост…
Спроси меня, зачем я эти годы в деле, брат,
Но, мерси, я той породы, что имеет свой бардак
В голове, мысли иначе, на волне песни и значит,
Я другой, теперь ответь мне, кто из нас домашний мальчик.
Эта культура по натуре моя победа,
На халтуре тут не вылезти, пойми, где бы ты не был,
Тебя знают люди, значит ты продукт употребляемый,
И сердце треском пополам, но ты неприкасаемый.
Обо мне говорят, ты не тот уже Shot,
На месте не могу стоять, нужен новый переворот,
Проекты, что я задумал, поверь, уже не мечты,
И мне плевать, пускай считают меня частью звезды.
Это не так, но у каждого своя голова,
Мы поднимаем флаг победы и она такова,
Полсотни треков за лето, три тэйпа — это успех,
Лови удачу, так, как я поймать когда-то успел…
