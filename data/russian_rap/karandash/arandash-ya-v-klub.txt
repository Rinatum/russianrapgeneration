

Исполнение: Денис Карандаш Григорьев
Продюсер: Денис Карандаш Григорьев
Текст: Денис Карандаш Григорьев
Scratch: Игорь DJ Superman Усков
Аранжировка: Александр shaMan Мигунов
Запись: Александр Крэк Винокуров
Сведение и мастеринг: Николай Nick Воронятов /New Tone Studio, Нижний Новгород/

Куплет 1
Я в клубе. Походкой разгоняю дыма клубы.
Под ультрафиолетом блестят зубы,
Блестят мои глаза, блестят стаканы в баре,
И на танцполе парень блестит тоже
Потной кожей среди девичьих ножек.
Мой пульс уничтожит виски, кола, текила вместе.
«Fendy», «Gucci», кошелёк залитый «Hennessy».
Тьфу! Фешенебельный притон.
Мимо прошла сумка «Луи Бьютон»
Очки «Prada» танцуют рядом.
Компанию составил, измазался помадой.
Свет яркий, Денису жарко.
R'n'B полюби, если денег не жалко.
И пусть голос сел, в горле сухо,
Я громче музыки кричу тебе на ухо.
Припев
Давай разнесём этот клуб!
Давай разнесём этот клуб!
Давай разнесём этот клуб!
Этот клуб! Этот клуб!
Этот клуб! Этот клуб!
Этот клуб! Этот клуб!
Этот клуб! Этот клуб!
Этот клуб! Этот...
Этот... клуб!
Куплет 2
Понимаешь мои чувства?
Я вижу только как джинсы «Kelvin Klein» трутся
Друг о друга. Их хозяйки прутся —
Кокаин в туалете, а потом снова туса.
«Shanel Коко» на ресницах,
Кто-то умудряется «Heineken'ом» в хлам напиться.
Ноль тридцать три — сто тридцать.
У меня есть деньги, но я не куплю из принципа.
Новый синтез продаёт кавказец.
Пара марок — мозг превращается в холодец.
Следуем дальше.
Я, как кусок льда, окружённый фаршем,
Холоден к силикону в модной майке.
Это «Versace»? Не признал, прости, зайка.
Я в дешёвом «Nik'е», прошлой коллекции,
Так что нам не спеться. Я скажу от сердца
Припев
Давай разнесём этот клуб!
Давай разнесём этот клуб!
Давай разнесём этот клуб!
Этот клуб! Этот клуб!
Этот клуб! Этот клуб!
Этот клуб! Этот клуб!
Этот клуб! Этот клуб!
Этот клуб! Е!
