
Slim (CENTR):
Туман над рекой, я жду на берегу,
Кинул монету в воду, она пошла ко дну,
И я сюда вернусь, если возьму высоту,
Я сюда вернусь, проверить глубину.
Пока они пилили нефтяную трубу,
Красные пятна появлялись на белом снегу,
И все опять по кругу, как кольца Сатурна,
Следы запутаны, утром синие губы.
Мне так легко, будто дали сандалии с крыльями,
Как «Bugatti Veyron», но из алюминия,
Как ракета «Булава» родом из России,
Этот альбом оснащен ядерным носителем.
Легко бросить слово, как карту на стол,
Легко любить просто себя самого,
Но за слово иногда нужно пояснить,
И больно видеть слезы на конце ее ресниц.
Алло, проснись! Смерть стоит того, чтоб жить,
Легко всех положить, когда нос в порошке,
Ты замешан, черные маски в дверном глазке,
Сломал ключ в замке из третьего на крышу аптеки.
Легкие деньги плавают в грязной воде,
Звучим убедительно, как приговор в суде,
Сам по себе в стае волков, мне так легко,
Я не опознан, как НЛО.
Туман над рекой, мне легко,
Ты брось мне лечить за пивко,
От скуки накидаю стихов,
Проход в ноги, рывок, это белый медведь поволок. (х2)
Словетский (Konstantah):
Это стих легче, чем апрель певчий,
Налегке, как те дети из регионов,
Кому даже гулять не в чем,
Если все будет по плану, то успех обеспечен.
Вечно беспечный. Стреляю речами в ветер,
Обычно под вечер, как-то более увесисто звучат во мраке те речи,
Безграничный диапазон без уздечек,
Мне давно еще было дано парить кречетом.
Когда Вы здесь, как можете Вы говорить, что догнаться нечем Вам?
Или мне видно, что не видно Вам, потому что я на пиджаке клетчатом,
Как сейчас не было еще легче, чем…
Застенчивый. Пока они видят только мясо в женщинах,
Если сильный думаешь будет легко?
Помни, что есть сила встречная,
Можешь кануть в никуда незамеченным.
В этой легкости есть что-то личное, мне с ней легко,
У нее легкий румянец на смуглом личике.
Митя Северный (Константа):
Ёу, Юго-запад сделал таким, воспитал и родил,
Теперь хочу капитал колотить,
Чтоб металл не коптил и ладони чтоб без мозолей,
Кашемир от «Collezioni».
Тут русский гамбино и рядом богиня войны и изобилия,
Теперь не в поисках смысла, а легкой наживы,
Немного догнали, немного пожили
От Пресни до Филы.
Меня все легче подорвать на рубилово,
Мне легко дается война.
Я над пропастью балансирую, если сорвусь, поднимать не нужно со дна,
Мне легче двигаться по низам.
Тут верхи с корней подрезать,
Не «садить», а «присядь», и не «если», а «когда», под огнем берега,
Да, я слегка на подъеме, на мусорскую волну не настроен,
И если врубаешь, то поймешь почему.
Да, тяжело на движении, легче будет только в гробу.
Я читаю про одно и то же, вижу одно и то же,
Слышу одно и то же, как ты,
Нами движет легкий профит, нами дышит Мефистофель,
Мне так легче в разы…
Rkp-Uno (1000 слов):
Представляю чисто русскую хандру,
Чисто русскую победу, чисто русскую беду.
Универсальный солдат уже на посту,
Микрофон в пыли, но я эту пыль пробью.
Не понимаешь, так и я не всех пойму,
Непонимание оставило дыру в черепе, не одну.
Свою линию гну, плюс точно бью,
Если ты поймешь то, что я «тру» — используешь в быту.
Все равно, что на столе – «Столичный» или «Брю»,
И все равно кто за столом – стукач или друг,
Ведь есть мужского пола много сук,
Не споришь, знаешь, от этих мух лучший трюк – правый хук.
Как Джеймс Кук мы открываем правду who is who,
Как Христофор Колумб причаливаем к берегу,
И кто бы ни пытался тут быть скрытным,
Его раскусят по-любому вечерком, зимой, на Мытной.
Словетский (Константа):
Туман над рекой, мне легко,
Ты брось мне лечить за пивко,
От скуки накидаю стихов,
Проход в ноги, рывок, это белый медведь поволок. (х2)
