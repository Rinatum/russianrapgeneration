
Вон, пешком, чуть ли не бегом катятся старые знакомые
За кольцом на палец или за дипломами.
Лишком напомнили довольными своими минами,
А мы с кентами синими и половины не успели.
А надо ли спирт на вату лить?
Надо тулить, куда светят фары?
Ждет от сына результатов уж седая мать.
Стараюсь не кумарить, появляться в хате чаще.
Не сыграть ни в ящик раньше времени, ни на баяне.
Пускай не на Майами, а на яме стать на ноги.
Бланки и счета бьют по местам паники.
Еще та нищета, а ведь уже не маленький.
Время пробивать варики, надевать галстуки, снимать валенки.
Завтра в канцелярии. В этот раз не плюну, по старому сценарию.
Сделаю заметку «созвониться с головой»,
Так как в папке «на потом» заводиться стала вонь.
Орган половой забивал, как гвоздь.
Забивал еще чего — дайте пощечину.
Ну вот. Минутка, и довольно мутных нот про завтра.
Режет порой, но вчера не затрахала Старая с косой.
Ответ «жив, здоров» на все приветы, «как дела?»,
Завтра и посмотрим, кто рана, а кто соль.
Режет порой, но вчера не затрахала Старая с косой.
Ответ «жив, здоров» на все приветы, «как дела?»,
Завтра и посмотрим, кто рана, а кто соль.
Заведем базар про завтра.
Не жду от автора джекпота.
Я с ним не здороваюсь, ему взаимно похуй.
Одолжит пожить — замирим, ей-богу.
До сих пор не понял, как это:
Кто со мной микрофон пользовал когда-то,
Потом перепудрил носик свой, вероятно,
Теперь носит погоны, по факту ментовские.
Впадлу чертовски рано или поздно
Плавно мутировать из одного в другого Лебовски.
Должен был стать надеждой и опорой,
А стал невеждой и засерой,
Что такое? Непозитивный стих я писал в Подмосковье,
Для кого деньги — не мотив, тот пиздлив больно.
После движений с животом я ощутил и понял:
Дороговизну гондонов в рэпе плохо кормят,
Надо ныкать бонг от лоха в форме,
Ибо стремные законы наши,
Жены и мамаши как кони пашут.
Хули, им страшно за завтра.
Обнажим шашки и дедовы трофейные вальтеры,
Пусть ответят за базар, кто кормил завтраками
Флаги подняты, глаза подними, пока идет сегодня.
Режет порой, но вчера не затрахала Старая с косой.
Ответ «жив, здоров» на все приветы, «как дела?»,
Завтра и посмотрим, кто рана, а кто соль.
Режет порой, но вчера не затрахала Старая с косой.
Ответ «жив, здоров» на все приветы, «как дела?»,
Завтра и посмотрим, кто рана, а кто соль.
