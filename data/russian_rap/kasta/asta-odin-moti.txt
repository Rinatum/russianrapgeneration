
Хамиль:
Всё дело в том, что вокруг все на такой серьёзке,
Нет, но я сам охотно пишу тексты жёсткие.
Но разговор доходит всегда до ступора,
Предложи я корешам добавить в песню юмора.
Так я оказался прижат своей идеей к стенке,
С весёлым настроением, не в своей тарелке.
Отчаяние безмерным было бы, если б не Змей,
Помню как свалился с неба этот речетативный рекордсмен.
Скандалит как-то парочка при мне,
Я встал так в стороне.
Гляжу и впрямь пацан на правильной волне.
Кричит ей что он змей, и что до него ей как до эвереста,
Оказалось это он читал подруге новый текст.
Всё так, теперь у нас один мотив,
Всё так, он мне как город побратит.
Ну да, этот тандем отлично слеплен,
Ну да, ведь наш общий настрой как степлер.
Змей:
Всё так, теперь у нас один мотив,
Всё так, он мне как город побратит.
Ну да, этот тандем отлично слеплен,
Ну да, ведь наш общий настрой как степлер.
Помню тот вечер...
Друг друга за руки держали,
А тут какой-то тип стоит и взглядом меня жалит.
Я напрягся, словно меня к стене прижали,
А ну, пойдём, поговрим за гаражами.
Сейчас то я конечно признаю свою вину,
Ну а тогда, ну чё ему, ещё б в рот заглянул.
Моей не нравится, когда я чуть что закипаю,
А как себя вести с такими наглыми типами?
Я ему что-то сказал тогда, он мне в обратку,
Дело дошло до кулаков, наш разговор был кратким.
Это сейчас то я знаю, что это был Хамиль,
Знал бы тогда, наверное б не хамил.
Всё так, теперь у нас один мотив,
Всё так, он мне как город побратит.
Ну да, этот тандем отлично слеплен,
Ну да, ведь наш общий настрой как степлер.
Хамиль:
Вот так пацаны...
