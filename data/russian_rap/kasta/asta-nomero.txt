
Номерок блатной три семерочки
Едет-катит фраерок – ксивы-корочки.
Номерок блатной да три семерочки
Не помогут тебе фраер твои корочки.
Модельная стрижка, борсетка подмышкой.
Заходит в ГАИ смазливый парнишка.
«Мы сегодня с папой были в салоне,
папа подарил мне новый Ландровер.
Нужны номера в реале.
Чтобы пацаны все охуевали,
Девчонки кричали, братки чтоб мычали.
А на постах мне менты честь отдавали».
Номерок блатной три семерочки
Едет-катит фраерок – ксивы-корочки.
Номерок блатной да три семерочки
Не помогут тебе фраер твои корочки.
На загородной трассе он,
Выжимает максимум на Ландровере – три семерки на номере.
Вздымая пыль, стаи гоняя по ветру.
Автомобиль достиг предела по спидометру.
Взмах черно-белого жезла и вот прекращен полет.
А пилот сжался вглубь кресла.
Но вспомнил о визитке прокурора области,
О пропуске в администрацию, дающих вольности.
«Вот они документы» — сказал как отрезал, — «Любишь эксперименты?
Сунь себе жезл»
Но судорога растеклась по корням волос,
Мент смотрел не пряча глаз, не тая злость.
Кокарда мерцала адским пламенем,
А под ней изо рта дуло ветром преисподней.
Шерсть в свете луны оклеймяло его контур,
Из чрева слова слышны: «Где техосмотр?»
Монтировкой, обросшей ворсом, он выдрал номер три семерки
С улыбкой борзой, не то рукой, не то копытом отдал честь
И исчез в машине ГАИ с номером 666.
Номерок блатной три семерочки
Едет-катит фраерок – ксивы-корочки.
Номерок блатной да три семерочки
Не помогут тебе фраер твои корочки.
