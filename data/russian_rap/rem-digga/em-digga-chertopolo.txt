
В городе замело все дороги
И как будто погиб здесь весь люд.
К бару потопали ноги немногих,
А мне как неохота туда где пьют и бьют.
Ветровку старую покажет снова тут герой,
Издали видно знак уставший цвета граната
Коли по воли судьбы занесло сюда тропой,
А до волос же раз приняло значит выпить тут надо.
Вперемешку с солью вода огнем не дарит тепло
И ставит на стол снова барменша мутное стекло
Как и надо так и травят тостом,
А потом грустим и про себя о том сложном но вроде самом простом.
Выжатый как лимон
И брошенный небрежно на тарелку как он,
Снова поддержанный кентом
Ведь мысли о ком,
Аппарат чей не доступен, он заложен мной тайком.
И как в пустыне путник верю люблю и жду
И как дурак купаю в луже мою мечту
И как верный пес губами я жмусь к бедру
И так обожаю что прямо слов не нахожу
И как в пустыне путник верю люблю и жду
И как дурак купаю в луже мою мечту
И как верный пес губами я жмусь к бедру
И так обожаю что прямо слов не нахожу
Выгреб я с сумки у неё все до копья
И даже возможно знаешь за горло может прижать как яд
Кишат так яро мысли но буду ведь я не я
А дурак ежели чудо мое погубит страшный маг
Вырос сволочью, скотиной я, вынес золото,
Играли скулы, но шатаюсь я, движимый голодом
К ломбарду с напарником причалил, добро я сдать
«Артем, братуха, за все вот это прошу я пять»
Вырыта могила мне была туда по правилам
И те дела когда мазда в бок ударила
Кровь потекла изо рта плюс пена там,
Авария, дорога, крики, темнота.
Выколите мне веки боже мой ну куда я смотрел
И посоветуйте что можно той сказать,
Которая в больничной палате, в моей кровати
Ад забыла весь и в слезах лезет целовать.
И как в пустыне путник верю люблю и жду
И как дурак купаю в луже мою мечту
И как верный пес губами я жмусь к бедру
И так обожаю что прямо слов не нахожу.
И как в пустыне путник верю люблю и жду
И как дурак купаю в луже мою мечту
И как верный пес губами я жмусь к бедру
И так обожаю что прямо слов не нахожу.
