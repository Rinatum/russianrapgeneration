
Братц, братц, ты мой братц, 
Братц, братц, ты мой братц. 
А ты не знал, а ты не знал, 
А я, а я твой братц, братц, ты мой братц, братц 
Ты мой братц. 
Купила тетя Ладу Калину, 
В кредит ее взяла и еще мобилу. 
Думает, что она мисс Самара, 
Водит так себе — жогает по тротуарам! 
Дядька в Беларусии не очень умен — 
Далек от молодежи, чувства юмора лишен. 
А папы брат ведет себя туповато — 
Я час ему втирал, что входящие бесплатно. 
А в деревне — ебнутый племянник 
Руками в солидоле поедает пряник. 
У брата сестра по папе сводная 
Для работы в овощной киоск пригодная. 
А сестры муж пиздец как бесит — 
Рубаха в джинсы, на пояс 3310. 
Моего двоюродного деда внук 
Любитель совокуплять некрасивых сук. 
Отчим моей крестной мамы 
В гости наведался часом невменяемый. 
Брат троюродный тему с магазином мутил — 
Честное ебало в обезьяннике светил. 
Как ни днюха, так гуляй цыгане! 
Чтобы поспать выноси из хаты пьяных. 
Просят постоянно: сыграй на пианино! 
Да я забыл ноты, курю я как скотина! 
Улыбался тете, видал ее на фоте, 
Типа я рад видеть, обнимите за животик. 
А дядиного брата дочь, но нет лица, 
Я бы ей втер, но она — родственница. 
Стой сестричка там пока на входе, 
Ты с деревни, а у нас в ботинках не ходят. 
Утолили жажду слюнявым Спрайтом — 
Мелкая сестра оставила на заднем. 
Братуха жестко палится на дискотеке 
По движениям видно — недавно он пришел с армейки. 
Он еще рыбак — курит он донской табак. 
А ты не знал, а ты не знал, 
А я, а я твой братц, братц, ты мой братц, братц 
Ты мой братц. 
Выса так-то в родне мажоров нет. 
И не оставил завещания твой прапрадед. 
Оставил грядку. За полдачи буду драться! 
Родственники гонят на халяву набухаться. 
Доярки, трактористы, швеи, машинисты, 
На круглой дате все тусуют чисто. 
Щипали щеки, дергали меня за ушки, 
Прикинь, на полтосолетие бабушки! 
Угощенье всем: тебе и мне, 
Селедка в шубе и салатик Оливье. 
Так-то помню тети Тамары дочку, 
Она ни че, но курит Яву, слушает Сердючку. 
Брат троюродный далек от клубной жизни, 
Олимпийка деда и похож на медведя Гризли! 
Есть еще артефакт — песни пел про воду, 
Сын Людмилы Паловны какой-то панк походу. 
А сестра стопудово из Балабольской области 
Матрац пихала в ухо, что мой френд на Фокусе. 
Что всё на фоксе, выходные в клубе... 
Клуба нет в твоей деревне, раскатала губы! 
Дядя Женя — мужчина лютый, терпкий, 
В магазе грузчик, фанат Охоты крепкой. 
Тети Люды муж — ППСМ-щик верный, 
Ха, я к нему на день рожденья не приеду! 
А сводный сын моего двоюродного дяди 
В автоматы все свои зарплаты тратит. 
А я такой холодный как айсберг в океане, 
Кручусь среди родни, как в мексиканском сериале. 
А ты не знал, а ты не знал, 
А я, а я твой братц, братц, ты мой братц, братц 
Ты мой братц.
