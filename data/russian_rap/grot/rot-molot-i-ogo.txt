
Научившись сиять, мы не осознанно побуждаем к этому всех окружающих, когда мы сами освобождаемся от своих страхов, мы автоматически освобождаем других.
Раздолбанный мафон, с плакатов надзирает Eminem
В холодной комнате, древо путей и вялых семенем
Тайна наивная, юная, но лишь бы отпустили
Отзвониться пацанам, по пробивать все ли там в силе
Пылью прошита одежда, распятие или троя
Греемся у костров, в недрах заброшенных строек
Пережидая метель, в корпусе старого кузова
На перекрестках пустоты, после распада союза
Сколько стояли как сталь, скольким пришлось сдаться
Границами миров, были коробки старых кварцев
Мы будь то бы одни, среди чужого моря ревущего
Обманывает компас, голосом телеведущего
Здесь безнадежно все, как создавать замки из пыли
Близкие дышали только тем, что друг у друга были
Смертельный риск, но мы в путь запрягали сани
Только догадываясь, чем в итоге это станет
Наша движуха, без названия и знамени
Ты тоже движешь, а не едешь сверху, значит ты с нами
Значит ты свой, хотя бы на одну сотую
Другие катаются и ботают, мы запускаем сердце
Город делает вдох, сегодня найти зарядку на смену сотовый сдох
Ночью будет холодно, взять подклад флисовый
Завтра утром на парах не засыпать, записывать
Смело нашвыривайся, возьми себе и даме
Мы тебя промоем и с мигалкой отвезем к маме
Ну как по венам, между хрущевскими корпусами
Каждый день проблемы супер державы, решаем сами
Думаешь наши машины жгут, ломают носы
Брейся, наши детально просчитывают посыл
Я народ на зуме, до электрона приближен
Значит народ воспитан, опрятен и подстрижен
От гремят низы, растворится зал
Быт, наш, вцепится, по одному
Это как босым бегать по углям
В книгах красочней, чем на яву
Нет батальных сцен и лихих погонь
Только грязь с 9 до 7
В этой кузнице молот и огонь
Смогут выковать нас людьми
Увесистый хук, 4 бэка в кучу
Это импульс и стимул, устремляющий к лучшему
Еще вчера мысль, клетка и ручка
Сегодня свет ослепительный, пронзающий тучи
Сюжеты закручены, какие тут шутки
Но, подмигнув в метро или стопнув попутки
Вчера это стикер, на телефонной будке
Сегодня квартал закрыт, по сотне пунктов
Знание юности, отчаяние взрослых
Стоим на своем, учится никогда не поздно
Вечно молодые, равнодушны к возрасту
Личностный рост, будет значимей должности
Объем возможности, равен числу подходов
Считаем летные часы, альбомы множим на годы
Вчера волна в двоичном коде, сегодня тысяча галочек больше чем годен
Никто за это,ничего не обещал нам
Был не по верно юн и порыв, он и дал начало
Годы неслись, мы наполняли смыслом
Все становилось серьезнее, с каждым записанным
Я стопорнулся на миг, распад, я один
Горизонты поблекли, ну все куда там не гони
Во круг пестрили эталоны, мутили все стильно
Глаза боялись, а руки делали что любили
Холодная комната, стала теплой я никуда не пропал
Не завязывал, я уходил за опытом
Когда то что написано, стало кому-то близко
Это согревало, и я снова брался за лист
Это только начало, теперь стопнуть никак
Алло! Братан, давай подловимся на фитах
Из пространства, на сетчатку липнут фрагменты
После от нас в микро
И прямиком к тебе.
От гремят низы, растворится зал
Быт, наш, вцепится, по одному
Это как босым бегать по углям
В книгах красочней, чем на яву
Нет батальных сцен и лихих погонь
Только грязь с 9 до 7
В этой кузнице молот и огонь
Смогут выковать нас людьми
