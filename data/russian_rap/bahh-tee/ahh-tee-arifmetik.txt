
Коли быть откровенным, то слова теряют цену,
Время не лечит, а калечит и пламя в душах тушит
В лужах... Орущие, что море по колено тонут
Таких типов топить только тапки залив бетоном
Больную спидом, наркоманией, алкоголизмом нацию
Несложно залошить, но есть нюансы
Даже среди этой серой массы есть пацы
Если попался, то расплатись на кассе
Есть несогласные, уставшие смотреть сквозь пальцы
Кому не все равно, ждущие команду «Фас»
И всех тех, кто своими темами барыжьи тазики
Качают. Готовы бить не в бровь, а в глаз.
Я бы убил его, это не холокост
Хотя хотелось, чтобы было все так же просто
Дело не в нации — я сам азербайджанец
Дело в жизнях, что сломал ты и твои братцы
По статистике в России на сотню девчонок
Девяносто восемь пацанов, из которых
Десять наркоманов, а из этих десяти
Восемь даже не доживут до тридцати
Ты меня не знаешь, я всего лишь один из
Тех, кто потерял близких. А ты — их убийца.
Сквозь призму отчаянья пытался докричаться до небес
Голосом пресным — это бесполезно...
Перестал бездействовать, когда меня коснулось,
Когда влетел в столб брат на своей «Пуле».
Слушал тру рэп и, как обычно, был накурен.
В девятнадцать лет ушел, забрав белокурую,
Что сидела рядом. Планировали свадьбу.
Ушел дядя десять дней спустя — страшно, правда?
Парень под кайфом на встречу, в хлам машина
И ровным счетом ничего, только след от шин
Говорил о скорости, о безысходности ситуации...
Судьба посмеялась — виновник жив остался
И дело вовсе не в барыжьих баксах,
Дело в жизнях, что сломал ты и твои братцы
По статистике в России на сотню девчонок
Девяносто восемь пацанов, из которых
Десять наркоманов, а из этих десяти
Восемь даже не доживут до тридцати
