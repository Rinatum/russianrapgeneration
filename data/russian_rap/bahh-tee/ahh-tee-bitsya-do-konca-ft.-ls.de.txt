
Когда сил нет, кулаки разбиты в кровь, в душе слеза
Победит лишь тот кто будеит биться до конца.
Когда за спиной километры, а до цели шаг.
Победит лишь тот кто будет биться до конца.
Биться до конца...
С пеленой в глазах я встаю на ринге
В который раз упав, в неравном поединке
И как его пустили… он же в два раза больше
Обидно… но мужчина, плакать не должен
Я весил 33… но… не ведал боли,
Лишь несправедливость комом стояла в горле
И в лютом гневе, не видя ничего вокруг
Я бью с криком «…» и ломаю руку
Первый нокаут… но было еще что-то кроме
Что трудно объяснить, но я это четко понял
И это стоило и значило больше намного
Ведь это действует, получше любого закона
Должен, значит, сможешь… запятые, где хочешь
Ставь, ведь только ты… можешь поставить точку.
На ринге, в жизни, потом или прямо сейчас
Ты сможешь найти силу быть одним из нас
Когда сил нет, кулаки разбиты в кровь, в душе слеза
Победит лишь тот кто будет биться до конца.
Когда за спиной километры, а до цели шаг.
Победит лишь тот кто будеит биться до конца.
Биться до конца...
Помню начало… мальчишку у больничной койки
Отец, которого учил бойцовской стойке
Ему расти в общагах, ведь после перестройки
Офицеры СССР брошены на помойках
Абсурд… врачи конечно строго против!
Но сын, либо ты боец, либо – не знаю кто ты
Здесь с недугом, потом со своими врагами,
Бейся до конца, иначе затопчут ногами
Так рос парень, которому не дали шансов,
Врачи, политики, конфликты на почве наций
Страна большая, мы разные, но все равны
Мы преданы вождям, которыми и преданы
Не плачь Россия, родина смутных времен
Бейся до конца и мы еще свое возьмем
Не вешай нос, братишка, финиш – далеко
Трудно, сложно… но… кому сейчас легко
Когда сил нет, кулаки разбиты в кровь, в душе слеза
Победит лишь тот кто будет биться до конца.
Когда за спиной километры, а до цели шаг.
Победит лишь тот кто будеит биться до конца.
Биться до конца...
