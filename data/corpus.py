import os

import torch
import pandas as pd
from tqdm import tqdm

from data.preprocessing import Preprocessor

END = '<end>'


class Dictionary(object):
    def __init__(self):
        self.word2idx = {}
        self.idx2word = []

    def add_word(self, word):
        if word not in self.word2idx:
            self.idx2word.append(word)
            self.word2idx[word] = len(self.idx2word) - 1
        return self.word2idx[word]

    def __len__(self):
        return len(self.idx2word)


class Corpus(object):
    def __init__(self, path):
        self.dictionary = Dictionary()
        self.preprocessor = Preprocessor()
        self.train = self.tokenize(os.path.join(path, 'train.csv'))
        self.valid = self.tokenize(os.path.join(path, 'test.csv'))

    def tokenize(self, path):
        songs = pd.read_csv(path, header=0)['song']
        clear = []
        for song in tqdm(songs):
            for line in song.split('\n'):
                if line != '' and '(' not in line and line != 'Припев':
                    clear.append(self.preprocessor(line) + [END])

        all_words = [word for line in clear for word in line]
        for word in all_words:
            self.dictionary.add_word(word)

        idss = []
        for line in clear:

            ids = []
            for word in line:
                ids.append(self.dictionary.word2idx[word])

            idss.append(torch.tensor(ids).type(torch.int64))

        ids = torch.cat(idss)

        return ids