import os

from bs4 import BeautifulSoup
from tqdm import tqdm
import requests


class RapTextRuParser:
    def __init__(self, out_dir, url='http://hip-hop.name/'):
        self.url = url
        self.out_dir = out_dir
        if not os.path.exists(self.out_dir):
            os.makedirs(self.out_dir)
        top50 = requests.get(self.url, allow_redirects=True).content
        self.soup = BeautifulSoup(top50, 'html.parser')

    def parse(self):

        for link in self.soup.find_all('a', href=True):
            songer = link['href']
            if '/text/' in songer:
                rapper = os.path.join(self.url, songer[1:])
                self.parse_rapper(rapper.split('/')[-2], rapper)

    def parse_rapper(self, rapper_name, rapper_url):
        print(rapper_name)
        content = requests.get(rapper_url, allow_redirects=True).content
        soup = BeautifulSoup(content, 'html.parser')
        for link in tqdm(soup.find_all('a', href=True)):
            try:
                if link.find(class_='album hidden-phone'):
                    song_next = link['href']
                    if 'album' in song_next:
                        continue
                    song_ref = os.path.join(self.url, song_next[1:])
                    rapper_dir = os.path.join(self.out_dir, rapper_name)
                    if not os.path.exists(rapper_dir):
                        os.makedirs(rapper_dir)
                    self.parse_song(song_ref, rapper_dir)

            except Exception:
                continue

    def parse_song(self, song_url, rapper_dir):
        song_content = requests.get(song_url, allow_redirects=True).content
        song_soup = BeautifulSoup(song_content, 'html.parser')
        song = song_soup.find_all(class_='entry')[0].text
        out_path = os.path.join(rapper_dir, song_url.split('/')[-2].replace(' ', '_')[1:-1]) + '.txt'
        with open(out_path, 'w+') as f:
            f.write(song)
