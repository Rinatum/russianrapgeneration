import torch
import numpy as np
from models.models import RNNModel, TransformerModel
from models.models import LSTM_BASE_PARAMS, GRU_BASE_PARAMS, TRANSFORMER_PARAMS

device = torch.device("cpu")


class GhostWriter:
    def __init__(self, brain='LSTM', weights_path='../weights/LSTM.pth', idx2word_path='../data/idx2word.npy'):
        self.brain = brain
        self.idx2word = np.load(idx2word_path)
        if brain in ['LSTM', 'GRU']:
            if brain == 'LSTM':
                self.model = RNNModel(**LSTM_BASE_PARAMS)
            else:
                self.model = RNNModel(**GRU_BASE_PARAMS)
        elif brain == 'Transformer':
            self.model = TransformerModel(**TRANSFORMER_PARAMS)
        else:
            raise Exception('Wrong model')

        self.model.load_state_dict(torch.load(weights_path,
                                              map_location=lambda storage, loc: storage), strict=False)

        self.model.eval()

    def generate(self, lines=8):
        input_ = torch.randint(1000, (1, 1), dtype=torch.long).to(device)
        if self.brain != 'Transformer':
            hidden = self.model.init_hidden(1)
        rap = []
        n_lines = 0
        with torch.no_grad():
            while n_lines < lines:
                if self.brain == 'Transformer':
                    output = self.model(input_, False)
                    word_weights = output[-1].squeeze().div(1.0).exp().cpu()
                    word_idx = torch.multinomial(word_weights, 1)[0]
                    word_tensor = torch.Tensor([[word_idx]]).long().to(device)
                    input_ = torch.cat([input_, word_tensor], 0)
                else:
                    output, hidden = self.model(input_, hidden)
                    word_weights = output.squeeze().div(1).exp().cpu()
                    word_idx = torch.multinomial(word_weights, 1)[0]
                    input_.fill_(word_idx)
                word = self.idx2word[word_idx]
                if word == '<end>':
                    n_lines += 1
                    word = '\n'
                rap.append(word)

        return rap
